const http = require('http');

const port = 4000;

const server = http.createServer(function(request, response){
	if (request.url == '/greeting'){
		//You can use request.url to get the current destination user in the browser. Yo can also check if the current destination of the user matches with the end point and if do the respective process for each endpoint
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hello Batch 197!')
	} else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the homepage!')
	} else{ // If none of the endpoint match the current destination of the user, return a default value like 'Page not available'
		response.writeHead(404,{'Content-Type': 'text/plain'})
		response.end('Page not available')
	}
})

server.listen(port);

console.log(`Server now accesible at localhost: ${port}`)