// 1. What directive is used by Node.js in loading the modules it needs?
// Answer: 

//require

// 2. What Node.js module contains a method for server creation?
// Answer: 

//let sample = http.createServer(function(request, response)


// 3. What is the method of the http object responsible for creating a server using Node.js?
// Answer: 

//createServer()

// 4. What method of the response object allows us to set status codes and content types?
// Answer:

//writehead

// 5. Where will console.log() output its contents when run in Node.js?
// Answer:

//terminal

// 6. What property of the request object contains the address's endpoint?
// Answer:

//url

// Simple Server

/*
	Instructions:

	- Import the http module using the required directive.
	- Create a variable port and assign it with the value of 3000.
	- Create a server using the createServer method that will listen in to the port provided above.
	- Console log in the terminal a message when the server is successfully running.
	- Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	- Access the login route to test if it’s working as intended.
	- Create a condition for any other routes that will return an error message.
	- Access any other route to test if it’s working as intended.

*/

//Code here:
const http = require('http');

const port = 3000;

const server = http.createServer(function(request, response){
	if (request.url == '/greeting'){
		//You can use request.url to get the current destination user in the browser. Yo can also check if the current destination of the user matches with the end point and if do the respective process for each endpoint
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('The server succesfully running!')
	} else if(request.url == '/loginpage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to login page!')
	} else{ // If none of the endpoint match the current destination of the user, return a default value like 'Page not available'
		response.writeHead(404,{'Content-Type': 'text/plain'})
		response.end('Error: Page not available..... Please insert the correct path!')
	}
})

server.listen(port);

console.log(`Server now accesible at localhost: ${port}`)